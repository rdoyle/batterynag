# i3/sway Low-Battery Notifier

# Install Instructions
## install the script into /usr/local/bin
```
sudo curl -o /usr/local/bin/_batterynag "https://gitlab.com/rdoyle/batterynag/raw/master/batterynag"
sudo chmod +x /usr/local/bin/_batterynag
```

## Add the execution of the checker to to sway/i3 config.
The first argument (10) is the battery percentage threshold to notify at. The second argument (60) is the polling interval in seconds. 
```
echo "exec /usr/local/bin/_batterynag 10 60" >> ~/.config/{sway,i3}/config
```
_You will have to direct this command to the correct config path depending on your wm._

## Reload config
```
swaymsg reload
i3-msg reload
```
_for i3, you'll likely need to restart the wm entireley. default keybinding is mod+shift+e_


